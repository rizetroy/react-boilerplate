import React, { Component } from 'react'
import {BrowserRouter,Route,Switch,Link} from 'react-router-dom'

import Home from './pages/Home'
import ListCharacter from './pages/ListCharacter'
import DetailCharacter from './pages/DetailCharacter'
import SearchCharacter from './pages/SearchCharacter'

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <ul>
          <li><Link to='/'>Home</Link></li>
          <li><Link to='/list'>ListCharacter</Link></li>
        </ul>
        <Switch>
          <Route path='/' component={Home} exact />
          <Route path='/list' component={ListCharacter} exact />
          <Route path='/search' component={SearchCharacter} />
          <Route path='/character/:uuid' component={DetailCharacter} />
        </Switch>
      </BrowserRouter>
    )
  }
}
