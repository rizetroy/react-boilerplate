import React, { Component } from 'react'
import axios from 'axios'


export default class DetailCharacter extends Component {
  constructor(props){
    super(props)
    this.state = {
      data: {}
    }
  }
  componentDidMount(){
    axios.get('https://rickandmortyapi.com/api/character/'+this.props.match.params.uuid).then(res=>{
      this.setState({data:res.data})
    })
  }
  render() {
    return (
      <div>
        {!this.state.data.id&&(
          <React.Fragment>
            Loading...
          </React.Fragment>
        )}
        {this.state.data.id&&(
          <div>
            <img src={this.state.data.image} alt={this.state.data.name} />
            <div>{this.state.data.name}</div>
          </div>
        )

        }
      </div>
    )
  }
}
