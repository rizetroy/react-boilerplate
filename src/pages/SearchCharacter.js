import React, { Component } from 'react'
import qs from 'qs'

export default class SearchCharacter extends Component {
  constructor(props){
    super(props)
    this.state = {
      query:''
    }
  }
  queryChange = (e)=>{
    const query = e.target.value
    this.setState({query})
  }
  doSearch = (e,query)=>{
    if(e.keyCode===13){
      this.props.history.push('/search?name='+query)
    }
  }
  render() {
    const data = qs.parse(this.props.location.search.slice(1))
    return (
      <div>
        Search {data.name}
        <div>
          <input type="text" onKeyDown={(e)=>this.doSearch(e,this.state.query)} onChange={this.queryChange} value={this.state.query} />
        </div>
      </div>
    )
  }
}
