import React, { Component } from 'react'
import axios from 'axios'
import {Row, Col, Container, Button} from 'reactstrap'

export default class ListCharacter extends Component {
  constructor(props){
    super(props)
    this.state = {
      data:{},
      next:'',
      prev:'',
      isLoading:true
    }
  }
  componentDidMount(){
    this.getData().then(data=>{
      this.setState({data,next:data.info.next,prev:data.info.prev,isLoading:false})
    })
  }

  getData = async(page)=>{
    const character = await axios.get(page!==undefined?page:'https://rickandmortyapi.com/api/character/')
    return character.data
  }
  buttonPress = async(page)=>{
    this.setState({isLoading:true})
    this.getData(page).then(data=>{
      this.setState({data,next:data.info.next,prev:data.info.prev,isLoading:false})
    })
  }
  goToDetail = (id)=>{
    this.props.history.push('/character/'+id)
  }

  render() {
    return (
      <Container>
        <Row className='justify-content-md-center'>
        {this.state.isLoading&&(
          <Col>Loading...</Col>
        )}
        {!this.state.isLoading&&
          <React.Fragment>
            {this.state.data.results.map((v,i)=>(
              <Col sm={{size:'auto'}} key={i.toString()}>
                <img onClick={()=>this.goToDetail(v.id)} src={v.image} alt={v.name} style={{display:'block'}}/>
                <div className='text-center'>{v.name}</div>
              </Col>
            ))}
          </React.Fragment>
        }
        </Row>
        <Row>
          <Col>
            <Button color='primary' onClick={()=>this.buttonPress(this.state.prev)}>Prev</Button>
            <span>&nbsp;</span><Button color='primary' onClick={()=>this.buttonPress(this.state.next)}>Next</Button>
          </Col>
        </Row>
      </Container>
    )
  }
}
